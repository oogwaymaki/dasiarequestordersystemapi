const rootpath = require ('app-root-path')
module.exports = {
		testEnvironment: 'node',
		rootDir :rootpath.path,
	    verbose: true,
		testMatch: [ "<rootDir>src/__tests__/**/*.test.js" ],
		setupFiles: [ "<rootDir>/jest/jest.init.js" ],
};