import { initiateOrderModel, insertWorkOrder, selectWorkOrder,selectWorkOrderSummary } from "../model/workorder";


export const createWorkOrder = async(workOrder,userName) => {
		await initiateOrderModel(userName);
		return await insertWorkOrder(workOrder,userName);
};


export const getListOfOrders = async(searchRequest, userName)=> {
		await initiateOrderModel(userName);
		return await selectWorkOrderSummary(searchRequest,userName);
};

export const getWorkOrder = async(workOrder,userName) => {
		await initiateOrderModel(userName);
		return await selectWorkOrder(workOrder,userName);
};