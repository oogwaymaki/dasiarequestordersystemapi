import { sign } from 'jsonwebtoken';
import passport from 'passport';
import mongoose from '../MongoDBCommunicator/connector';
import  bcrypt from 'bcryptjs';


import  { ExtractJwt, Strategy } from 'passport-jwt';
import  { userModel, createUser,findUserModel } from '../model/user';

const numbers = [1, 2, 3];
const realNumbers = [0, ...numbers];
const secretKey = 'secretKey'

const passportOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: secretKey
};




export const registerUser =  async (authentictionRegisitration) => {
     return await createUser(authentictionRegisitration,userModel);
};

export const findUser =async (searchRequest) => {
    return await findUserModel(searchRequest,userModel);
}
export const authenticateUser = async (username, password) => {
	console.log(username);
	console.log(password);
    if (!username || !password)
        return null
    // here you would normally fetch the user from your database
    let multipleUser = await findUser({username: username },userModel)
    let authUser = multipleUser[0];
    console.log('-------')
    console.log(authUser);
    if (authUser.username.toLowerCase() != username.toLowerCase() ||  bcrypt.compareSync(password,authUser.password) != true)
      {  console.log('oh oh' + authUser.username)
        return null}        
    const token = sign({ username: authUser.username }, secretKey, { expiresIn: 60 * 60 * 24 });
    return Promise.resolve(token);
};
const jwtStrategy = new Strategy(passportOptions, async (jwt_payload, done) => {
    const username = jwt_payload.username;
    // here you would normally fetch the user from your database
    let multipleUser = await findUser({username: username },userModel)
    let authUser = multipleUser[0];
    console.log(authUser);
    if (authUser.username !== username) {
        done(new Error('User not found'), null)
    } else {
        delete authUser.password;
        done(null, authUser)
    }
});

passport.use(jwtStrategy);

export const mypassport = passport;
