import  mongoose from  '../MongoDBCommunicator/connector';
import  bcrypt from 'bcryptjs';
import {  buildingSuperId,createBusinessSuperWithId,findSuperModel} from './buildingSupers';
import { createBuildingWithId,buildingModel,findBuildings} from './buildings';
import  { userModel, createUser,findUserModel } from './user';
import { createResidentialWithId,residentialModel,findResidentialModel } from './residentInformation'
import { initiateTechnitionModel } from './technition'
import { customLogger } from '../logger/timestamp_logging';
import autoIncrement from 'mongoose-auto-increment'
autoIncrement.initialize(mongoose);
export var orderWorkModel = orderWorkModel || {};


const initiateWorkSchema = (companyId) => {
return mongoose.Schema({
	    urgent: Boolean,
	    type: String,
		unit: String,
		titleSummary: String,
		orderId: {
			type: Number,
			index: true
		},
		status: {
			type: String,
			index: true
		},
		technitions:[  { type: mongoose.Schema.ObjectId, ref: 'technitions'}	 ],
		address:  { type: mongoose.Schema.ObjectId, ref: 'Buildings_' + companyId} ,
	 	userRecord: [ { type: mongoose.Schema.ObjectId, ref: 'UserSchema' } ],
		businessSuper: [ { type: mongoose.Schema.ObjectId, ref: 'BuildingSupers_'  + companyId}],
		residentInformation: [{ type: mongoose.Schema.ObjectId, ref: 'residentinformation_' + companyId} ],
		workDescription: String,
		comments: [{
			id : Number,
			createdAt: Date,
			commentEntry : String
		}],
		inActive: Boolean,
		createdBy: { type: mongoose.Schema.ObjectId, ref: 'UserSchema' },
		createdAt: Date,
  		updatedAt: Date
});
}
export const initiateOrderModel = (user) => {
  let orderWorkSchema =initiateWorkSchema(user.companyId)

  orderWorkSchema.plugin(autoIncrement.plugin, { model: 'WorkOrder_' + user.companyId, field: 'orderId' });
  orderWorkSchema.pre('save', function (next) {
      let now = Date.now()
     this.updatedAt = now
  // Set a value for createdAt only if it is null
     if (!this.createdAt) {
         this.createdAt = now
         this.status = "Open";
     }
   // Call the next function in the pre-save chain
  		next();
    });

  orderWorkSchema.pre('find', async function () {

	let keys = this.getPopulatedPaths();
	customLogger.log({level:"debug","prefind" : keys ,"Session" : "orderworkschema"});
	customLogger.log({level:"debug","prefind" : this.getQuery()	,"Session" : "orderworkschema"});
	let query = this.getQuery();
	if  (typeof query.streetAddress !== 'undefined' && query.streetAddress !== null) {
		let building = await buildingModel[query.companyId].find({ streetAddress: query.streetAddress});
	 	if (building.length > 0)
	 		delete query['streetAddress'];
	 		delete query['companyId'];
	 		query['address'] = building[0]._id;
	 		console.log(query);
	    	this.setQuery(query);
	}
	else {
		delete query["companyId"];
		console.log(query);
		this.setQuery(query);
	}
});

 if (!orderWorkModel[user.companyId])
	 	orderWorkModel[user.companyId] =  mongoose.model('WorkOrder_'+ user.companyId, orderWorkSchema);
	 customLogger.log({level:"debug","message": "Called  createBuildingSchema with " + user });
};



export const insertWorkOrder = async(workOrder,user) => {
	customLogger.log({level:"debug","AddCondition" : workOrder ,"Session" : user});

	createResidentialWithId(user);
	createBuildingWithId(user);
	createBusinessSuperWithId(user);
	initiateTechnitionModel();
  var ObjectId = mongoose.Types.ObjectId;
   return new Promise(async function(resolve, reject) {
      let usersAvailable = await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
             resolve({created: false,error:"user does not exist or an error has occurred"});
           return;
       }
      customLogger.log({level:"debug","AddCondition" : usersAvailable ,"Session" : user});
      let  buildingForWorkOrder = await findBuildings({ streetAddress: workOrder.streetAddress }, buildingModel[user.companyId]);
      customLogger.log({level:"debug","FindCondition" : workOrder,"Session" : user});

      if (buildingForWorkOrder.length < 1) {
       	 	  resolve({created: false,error:"building does not exists"});
       	 	return;
       }
     customLogger.log({level:"debug","FindCondition" : buildingForWorkOrder,"Session" : user});

     let residentialForWorkOrder = await findResidentialModel({ unitNumber: workOrder.unit , livingAddress: buildingForWorkOrder[0]._id },residentialModel[user.companyId]);
     customLogger.log({level:"debug","FindConditionResidential" : residentialForWorkOrder,"Session" : user});

     if (residentialForWorkOrder.length < 1) {
       	 	resolve({created: false,error:"residential information does not exists"});
       	 	return;
       }

	let workOrderEntry = new orderWorkModel[user.companyId](workOrder);
	       	   workOrderEntry.userRecord = new ObjectId(usersAvailable[0]._id)
   	     	   workOrderEntry.residentInformation = [];
	       	   workOrderEntry.businessSuper = [];
	       	   for (let residentialSingle of residentialForWorkOrder) {
	       	   		console.log(residentialSingle._id);
	       	   	   workOrderEntry.residentInformation.push(new ObjectId(residentialSingle._id));
	       	   }
	       	   for (let businessSuperID of buildingForWorkOrder[0].buildingSupervisor) {
	       	   	   customLogger.log({level:"debug",businessSuperID});
	       	   	   workOrderEntry.businessSuper.push(new ObjectId(businessSuperID));
	       	   }
	       	   workOrderEntry.address = new  ObjectId(buildingForWorkOrder[0]._id);
	           workOrderEntry.save();
	           resolve({created: true});
	           return;
     });



  };
export const updatetWorkOrder = async(workOrder,user) => {
	customLogger.log({level:"debug","AddCondition" : workOrder ,"Session" : user});

	createResidentialWithId(user);
	createBuildingWithId(user);
	createBusinessSuperWithId(user);
	initiateTechnitionModel();
	
  var ObjectId = mongoose.Types.ObjectId;
   return new Promise(async function(resolve, reject) {
      let usersAvailable = await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
             resolve({created: false,error:"user does not exist or an error has occurred"});
           return;
       }

       let workOrderAvailable = await findWorkOrder({ orderId: workOrderId.orderId},orderWorkModel[user.companyId]);
	   if (workOrderAvailable.length < 1) {
       	 	  resolve({update: false,error:"Work Order does not exist"});
       	 	return;
       }

  });
}

export const selectWorkOrderSummary = async(searchRequest,user) => {
	customLogger.log({level:"debug","SearchCondition" : searchRequest ,"Session" : user});

	createResidentialWithId(user);
	createBuildingWithId(user);
	createBusinessSuperWithId(user);
	initiateTechnitionModel();

  var ObjectId = mongoose.Types.ObjectId;
   return new Promise(async function(resolve, reject) {
      let usersAvailable = await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
             resolve({created: false,error:"user does not exist or an error has occurred"});
           return;
       }

       let workOrderAvailable = await  lookUpWorkOrderWithFields(searchRequest,orderWorkModel[user.companyId],
       	['urgent','type','orderId','titleSummary','technitions','address','unit'],user)

       if(workOrderAvailable.length > 0 )
      		 resolve({found: true, workOrder: workOrderAvailable});
       else
             resolve({found: false, workOrder: workOrderAvailable});
  });
}


export const selectWorkOrder = async(workOrder,user) => {
	customLogger.log({level:"debug","AddCondition" : workOrder ,"Session" : user});

	createResidentialWithId(user);
	createBuildingWithId(user);
	createBusinessSuperWithId(user);
	initiateTechnitionModel();

  var ObjectId = mongoose.Types.ObjectId;
   return new Promise(async function(resolve, reject) {
      let usersAvailable = await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
             resolve({created: false,error:"user does not exist or an error has occurred"});
           return;
       }

       let workOrderAvailable = await lookUpWorkOrder({ orderId: workOrder.orderId},orderWorkModel[user.companyId]);
	   if (workOrderAvailable.length < 1) {
       	 	  resolve({update: false,error:"Work Order does not exist"});
       	 	return;
       }

       resolve({found: true, workOrder: workOrderAvailable});
  });
}

export const lookUpWorkOrder = ( searchRequest,superModel) => {
 return new Promise(function(resolve, reject) {
 	console.log(superModel)
 	 customLogger.log({level:"debug","LookUp" : searchRequest });
    resolve(superModel.find(searchRequest)
    	.populate({ path: 'residentInformation',
    		select: ['-userRecord','-livingAddress']
    })
    	.populate('userRecord')
    	.populate('businessSuper')
    	.populate('Technitions')
    	.populate({path:
    		'address',select:['-buildingSupervisor','-addedBy'],
    	}));
   });
}

export const lookUpWorkOrderWithFields = ( searchRequest,superModel,fields,user) => {
 return new Promise(function(resolve, reject) {
 	console.log(superModel)
 	customLogger.log({level:"debug",messge: { "LookUp" : searchRequest }});
 	searchRequest['find']['companyId'] = user.companyId;

 	resolve(superModel.find(searchRequest.find,fields)
 	   	.populate({ path:  'residentInformation',
    		select: ['-userRecord','-livingAddress']
    })
    	.populate({path: 'userRecord'})
    	.populate('businessSuper')
    	.populate('Technitions')
    	.populate({path:'address',
    		select:['-buildingSupervisor','-addedBy']
    	}));
 });
};

export const findWorkOrder = ( searchRequest,superModel) => {
 return new Promise(function(resolve, reject) {
 	console.log(superModel)
 	superModel.find(searchRequest ,function (err, users) {
      resolve(users);
   });
  })
};





