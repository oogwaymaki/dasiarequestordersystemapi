	import  mongoose from  '../MongoDBCommunicator/connector';
import  { findUserModel } from './user';
import { customLogger} from '../logger/timestamp_logging';

let initializeBuildingSupers =  function() {
		return mongoose.Schema({
			firstName: {
				type: String,
				index: true
			},
			lastName:{
				type: String,
				index: true
			},
			phoneNumber: String,
			email: String,
			inActive: Boolean,
		 	userRecord:  { type: mongoose.Schema.ObjectId, ref: 'UserSchema' },
		 	createdAt: Date,
	  		updatedAt: Date	
	});
}

export var buildingSuperId = buildingSuperId || {}
export const createBusinessSuperWithId =  (user) => {
	let buildingSuperSchema = initializeBuildingSupers();
	 buildingSuperSchema.pre('save', function (next) {
	let now = Date.now()
	this.updatedAt = now
	// Set a value for createdAt only if it is null
	if (!this.createdAt) {
	this.createdAt = now
	}
	// Call the next function in the pre-save chain
	next();
	})
	if (!buildingSuperId[user.companyId])
	 	buildingSuperId[user.companyId] =  mongoose.model('BuildingSupers_'+ user.companyId, buildingSuperSchema);
	 customLogger.log({level:"debug","message": "Called  createBuildingSchema with " + user });
}

export const createBuildingSupers = async(newUser,user) => {
	customLogger.log({level:"debug","AddCondition" : newUser ,"Session" : user});
 
  var ObjectId = mongoose.Types.ObjectId;
   return new Promise(async function(resolve, reject) {
      let usersAvailable= await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
             resolve({created: false,error:"user does not exist or error as occurred"});
           return;
       }

      let  userAlreadyInserted = await findSuperModel({ userRecord: usersAvailable[0]._id }, buildingSuperId[user.companyId]);
      if (userAlreadyInserted.length > 0 ) {
 	     	customLogger.log({level:"error","AddCondition" : newUser ,"Session" : user,"message":"User already exists"});
       	    resolve({created: false,error:"super user already exists"});
       	 	return;
       }

       let buildingSuper = new buildingSuperId[user.companyId](newUser);
       	   buildingSuper.userRecord = new ObjectId(usersAvailable[0]._id);
           buildingSuper.save();
           resolve({created: true});
           return;
     });
  };

export const findSuperModel = ( searchRequest,superModel) => {
 return new Promise(function(resolve, reject) {
 	console.log(superModel)
 	superModel.find(searchRequest ,function (err, users) {
      resolve(users);
   });
  })
};


