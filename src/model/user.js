import  mongoose from  '../MongoDBCommunicator/connector';
import { findCompanyModel } from './company'
import  bcrypt from 'bcryptjs';

let UserSchema = mongoose.Schema({
  username: {
    type: String,
    index: true
  },
  password: {
  type: String,
  select: false
  },
  email: {
    type: String,
    index: true
  },
  role: {
  type: String,
    index: true
  },
  inActive: Boolean,
  companyId: { type: mongoose.Schema.ObjectId, ref: 'companies' }
});

UserSchema.pre('save', function (next) {
  let now = Date.now()
  this.updatedAt = now
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next();
})

export const userModel =  mongoose.model('UserSchema', UserSchema);
export const createUser = (newUser,userModal) => {
     let now = Date.now()
   return new Promise(function(resolve, reject) {
  let result = false;
   bcrypt.genSalt(10, function(err, salt) {
       bcrypt.hash(newUser.password, salt, async function(err, hash) {
      newUser.password = hash;
      console.log(newUser.companyName);
      let users = await findUserModel({ username: newUser.username} ,userModel);
       if  (users.length === 0) {
           console.log("Username supplied Failed"+newUser.username);
           let userObject = new userModal(newUser);
            let ObjectId = mongoose.Types.ObjectId;
           if (newUser.companyName != null) {
                let companiesFound = await  findCompanyModel({companyName: newUser.companyName});
                console.log(companiesFound);
                if  (companiesFound.length > 0) {
                    userObject.companyId = new ObjectId(companiesFound[0]._id);
                  }
               else {
                resolve({created: false,error:"company does not exist"});
                 return;
               }
           }
               userObject.save();
          resolve({created: true });
    }
    else  {
       resolve({created: false,error:"user already exists or an error as occurred"});
      }
     });
  });
 })
};

export const findUserModel = (searchRequest) => {
return new Promise(function(resolve, reject) {
   resolve(userModel.find(searchRequest).select("+password"));
  })
};



