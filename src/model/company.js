import  mongoose from  '../MongoDBCommunicator/connector';
import  { userModel, createUser,findUserModel } from './user';

let initalizeCompany = function() {
		return mongoose.Schema({
		    companyName : {
		    	type: String,
		    	index: true,
		    },
		    owner: String,	
			phoneNumber: String,
			email: String,
		 	userCreated:  { type: mongoose.Schema.ObjectId, ref: 'UserSchema' } ,
		 	inActive: Boolean,		
		 	createdAt: Date,
	  		updatedAt: Date
	});
}


let createModel = () => {
		var companySchema = initalizeCompany();
		companySchema.pre('save', function (next) {
	  let now = Date.now()
	  this.updatedAt = now
	  // Set a value for createdAt only if it is null
	  if (!this.createdAt) {
	    this.createdAt = now
	  }
	  // Call the next function in the pre-save chain
	  next();
	});
   return mongoose.model('Company', companySchema)
};
export const companyModel = createModel();
export const createCompany = (company,userName) =>{
 let now = Date.now()

   return new Promise(async function(resolve, reject) {
   	// Lookup user

	      let users = await findUserModel({ username: userName});
// if found user create company name
	       if  (users.length > 0) {
	       	  let companysFound = await findCompanyModel({companyName: company.companyName});
	       	  if (companysFound.length > 0 ) {
	       	  resolve({created: false,error:"company already exists"});
	       	  	return;
	       	  }
	          let companyObject = new companyModel(company);
	          	companyObject.save();
	         resolve({created: true});
	          }   
	    	else  {
	       		resolve({created: false,error:"user does not exist or error as occurred"});
	       		return;
	      	}
	 });
};

export const findCompanyModel = (searchRequest) => {
 return new Promise(function(resolve, reject) {
  companyModel.find(searchRequest ,function (err, users) {
      resolve(users);
   });
  })
};
