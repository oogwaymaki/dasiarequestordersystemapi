import  mongoose from  '../MongoDBCommunicator/connector';
import  bcrypt from 'bcryptjs';
import { createBuildingWithId,buildingModel,findBuildings} from './buildings';
import  { userModel, createUser,findUserModel } from './user';

let createResidentialSchema  = function(companyId)
{
return mongoose.Schema({
		firstName: {
			type: String,
			index: true
		},
		lastName:{
			type: String,
			index: true
		},
		phoneNumber: String,
		email: String,
		unitNumber: String,
		livingAddress: { type: mongoose.Schema.ObjectId, ref: 'Buildings_' + companyId  },
	 	userRecord:  { type: mongoose.Schema.ObjectId, ref: 'UserSchema'},
	 	createdAt: Date,
    updatedAt: Date,
    companyId: { type: mongoose.Schema.ObjectId, ref: 'companies' }
});
};

export var residentialModel = residentialModel ||  {};

export const createResidentialWithId =  (user) => {
	let residentialSchema = createResidentialSchema(user.companyId);

	residentialSchema.pre('save', function (next) {
	let now = Date.now()
	this.updatedAt = now
	// Set a value for createdAt only if it is null
	if (!this.createdAt) {
	this.createdAt = now
	}
	// Call the next function in the pre-save chain
	next();
	})
	if (!residentialModel[user.companyId])
	 	residentialModel[user.companyId] =  mongoose.model('residentinformation_'+ user.companyId, residentialSchema);
	console.log("Called Residential Model Id" + Object.keys(residentialModel));
}

export const createResidential = (newUser,user) => {
  var ObjectId = mongoose.Types.ObjectId;
    createBuildingWithId(user);
   return new Promise(async function(resolve, reject) {
      let usersAvailable= await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
           resolve({created: false,error:"user does not exist or error as occurred"});
           return;
       }

      let  userAlreadyInserted = await findResidentialModel({ userRecord: usersAvailable[0]._id }, residentialModel[user.companyId]);
      if (userAlreadyInserted.length > 0 ) {
       	 resolve({created: false,error:"user already exists in residential model"});
       	 	return;
       }
       console.log(user.streetAddress);
      let  doesPlaceExist = await findBuildings({ streetAddress: newUser.streetAddress }, buildingModel[user.companyId]);
      console.log(doesPlaceExist);
      if (doesPlaceExist.length < 1) {
       	 resolve({created: false,error:"building does not exist"});
       	 	return;
       }

       let resident = new residentialModel[user.companyId](newUser);
       	   resident.userRecord = new ObjectId(usersAvailable[0]._id)
       	   resident.livingAddress = new ObjectId(doesPlaceExist[0]._id);
           resident.companyId = new ObjectId(user.companyId);
           resident.save();
           resolve({created: true});
           return;
     });
  };

export const findResidentialModel = ( searchRequest,superModel) => {
 return new Promise(function(resolve, reject) {
 	console.log(superModel)
 	superModel.find(searchRequest ,function (err, users) {
      resolve(users);
   });
  })
};



