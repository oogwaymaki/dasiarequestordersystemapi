import  mongoose from  '../MongoDBCommunicator/connector';
import  bcrypt from 'bcryptjs';
import { findCompanyModel } from './company'
import  { userModel, createUser,findUserModel } from './user';
import { customLogger} from '../logger/timestamp_logging';

const initiateTechnitionSchema = () => {
	return  mongoose.Schema({
			firstName: {
				type: String,
				index: true
			},
			lastName:{
				type: String,
				index: true
			},
			companyId: { type: mongoose.Schema.ObjectId, ref: 'companies' },
			inActive: Boolean,
			phoneNumber: String,
			specialities : [ String ],
			type: String,
			email: String,
		 	userRecord: { type: mongoose.Schema.ObjectId, ref: 'UserSchema' },
		 	createdAt: Date,
	        updatedAt: Date
	});
}
export var technitionModel = technitionModel || null;
export const initiateTechnitionModel = () => {
let technitionModelSchema =  initiateTechnitionSchema();
	technitionModelSchema.pre('save', function (next) {
	let now = Date.now();
	this.updatedAt = now
	// Set a value for createdAt only if it is null
	if (!this.createdAt) {
	this.createdAt = now
	}
	// Call the next function in the pre-save chain
	next();
	})
	  if (!technitionModel)
		technitionModel =  mongoose.model('Technitions',technitionModelSchema);
}


export const createTechnition = (newUser,user) => {
  var ObjectId = mongoose.Types.ObjectId;
   return new Promise(async function(resolve, reject) {
      let usersAvailable= await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
           resolve({created: false,error:"user does not exist or error as occurred"});
           return;
       }

      let  companyAvailable = await findCompanyModel({ _id: user.companyId });
      customLogger.log({level: "debug",msg:"Found company" + companyAvailable});
      if (companyAvailable.length < 1 ) {
       	 resolve({created: false,error:"company does not exist"});
       	 	return;
       }

      let  userAlreadyInserted = await findTechnitionModel({ userRecord: usersAvailable[0]._id },technitionModel);
      if (userAlreadyInserted.length > 0 ) {
       	 	resolve({created: false,error:"technition already exists"});
       	 	return;
       }

       let technition = new technitionModel(newUser);
       	   technition.userRecord = new ObjectId(usersAvailable[0]._id);
       	   technition.companyId = new ObjectId(companyAvailable[0]._id);
           technition.save();
           customLogger.log({level: "debug",msg: "Saved Object:" + newUser});
           resolve({created: true});
           return;
       })
};

   export const findTechnitionModel = ( searchRequest,superModel) => {
 	return new Promise(function(resolve, reject) {
 	console.log(superModel)
 	superModel.find(searchRequest ,function (err, users) {
      resolve(users);
   });
  })
 };
