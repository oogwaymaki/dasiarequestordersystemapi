	import  mongoose from  '../MongoDBCommunicator/connector';
import  { findUserModel }  from './user';
import {  buildingSuperId,createBusinessSuperWithId,findSuperModel} from './buildingSupers';
import { customLogger } from '../logger/timestamp_logging';

export const initializeBuilding  =  function(companyId) {
		return mongoose.Schema({
			streetAddress:{
				type: String,
				index: true
			},
			inActive: Boolean,			
			type: String,
			buildingSupervisor: [ { type: mongoose.Schema.ObjectId, ref: 'Buildings_' + companyId  }],
			addedBy : {
				  type: mongoose.Schema.ObjectId, ref: 'UserSchema'
			},
			createdAt: Date,
	  		updatedAt: Date
	});
}

export var buildingModel= buildingModel || {}

export const createBuildingWithId = (user) => {
	let buildingSuperSchema =  initializeBuilding(user.companyId);
	buildingSuperSchema.pre('save', function (next) {
	let now = Date.now()
	this.updatedAt = now
	// Set a value for createdAt only if it is null
	if (!this.createdAt) {
	this.createdAt = now
	}
	// Call the next function in the pre-save chain
	next();
	})
	  if (!buildingModel[user.companyId])
		buildingModel[user.companyId] =  mongoose.model('Buildings_'+ user.companyId, buildingSuperSchema);
}

export const createBuilding = (building,user) => {

  createBusinessSuperWithId(user);
  var ObjectId = mongoose.Types.ObjectId; 
   return new Promise(async function(resolve, reject) {
      let usersAvailable= await findUserModel({ username: user.username});
       if  (usersAvailable.length === 0 || usersAvailable.length > 1) {
          		resolve({created: false,error:"user does not exist or error as occurred"});
           return;
       }

       let arrayUserIds = [];

      for (const singleUser of building.supers) {
	      let  userAlreadyInserted = await findSuperModel({ email: singleUser	 },buildingSuperId[user.companyId])
	      if (userAlreadyInserted.length < 1) {
	       	 	resolve({created: false,error:"super does not exist"});
	       	 	return;
	       }
		  arrayUserIds.push(new ObjectId(userAlreadyInserted[0]._id));
  	  }
  	   let buildingAlreadyInserted  =  await findBuildings({ streetAddress: building.streetAddress},buildingModel[user.companyId])

      	if (buildingAlreadyInserted.length > 0 ) {
				resolve({created: false,error:"Building already inserted"});
	       	 	return;
      	}

       let buildings = new buildingModel[user.companyId](building);
       	   buildings.addedBy = new ObjectId(usersAvailable[0]._id);
       	   for (const user of arrayUserIds){
       	  	 buildings.buildingSupervisor.push(user);
       	   }
           buildings.save();
           resolve({created: true});
           return;
     });
  };

export const findBuildings = ( searchRequest,buildingsObject) => {
 return new Promise(function(resolve, reject) {
  buildingsObject.find(searchRequest ,function (err, users) {
  	console.log(users);
      resolve(users);
   });
  })
};



