import  { createBuildingSupers,createBusinessSuperWithId } from '../model/buildingSupers';
import { createBuilding,createBuildingWithId } from  '../model/buildings';
import { createCompany } from '../model/company';
import {createResidentialWithId , createResidential} from '../model/residentInformation';
import { initiateTechnitionModel,createTechnition } from  '../model/technition';


export const registerBusinessSuper = async(superRecord,userName) => {
		await createBusinessSuperWithId(userName);
		return await createBuildingSupers(superRecord,userName);
};

export const registerBuilding = async(buildingRecord,userName) => {
		await createBuildingWithId(userName);
		return await createBuilding(buildingRecord,userName);
};
export const registerCompany = async(company,userName) => {
		return await createCompany(company,userName)
}

export const registerTechnition = async(technition,userName) => {
		await initiateTechnitionModel();
		return await createTechnition(technition,userName)
}

export const registerResidential = async(company,userName) => {
	    await createResidentialWithId(userName);
		return await createResidential(company,userName)
}