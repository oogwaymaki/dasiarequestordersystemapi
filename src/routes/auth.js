import express from 'express';
import { authenticateUser,registerUser,findUser } from '../auth/authImplementation';
import ash from 'express-async-handler';
import { mypassport }  from '../auth/authImplementation';

var router = express.Router();

router.get('/findusers', mypassport.authenticate('jwt', { session: false }), ash(async(req, res) => {
    const users = await findUser({role: req.body.role});
    res.json(users);
}));

 router.post('/signup', ash(async(req, res) => {
  // handle user authentication
    console.log(req.body);
    const username = req.body.username;
    const password = req.body.password;
    const phone = req.body.phone;
    const emailAddress = req.body.email;
    const role = req.body.role;
    const wasCreated =  await registerUser({
    	username: username,
    	password: password,
    	phoneNumber: phone,
    	email: emailAddress,
      companyName: req.body.companyName,
      role: role
    });
    res.json(wasCreated);
}));

router.post('/authenticate', ash(async(req, res) => {
  // handle user authentication
    console.log(req.body);
    const username = req.body.username;
    const password = req.body.password;
    const token = await authenticateUser(username, password);
    if (!token)
        return res.json({ success: false, msg: 'Wrong username or password' });
    return res.json({ success: true, token });
}));

export default router;

