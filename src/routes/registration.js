import express from 'express';
import { registerBusinessSuper,registerBuilding,registerCompany,registerResidential ,registerTechnition} from '../register/registerBusinessSuper';
import ash from 'express-async-handler';
import { mypassport }  from '../auth/authImplementation';
var router = express.Router();

router.post('/registerResident', mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if  (req.user.role === null){
 		return res.json({msg:  "Sorry" + "Role" + req.user.role})
 	}

    const wasCreated =  await registerResidential({
    	firstName: req.body.firstName,
    	lastName: req.body.lastName,
    	phoneNumber: req.body.phoneNumber,
    	unitNumber: req.body.unitNumber,
    	streetAddress: req.body.streetAddress
    },req.user);
      console.log(req.body.streetAddress);
      res.json(wasCreated);
}));


router.post('/registerCompany', mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if  (req.user.role !== "admin"){
 		return res.json({msg:  "Sorry" + "Role" + req.user.role})
 	}

    const wasCreated =  await registerCompany({
    	owner: req.body.owner,
    	email: req.body.email,
    	phoneNumber: req.body.phoneNumber,
    	companyName: req.body.companyName
    },req.user.username);
  res.json(wasCreated);
}));



router.post('/registerBuilding', mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if (req.user.role !== "super" && (req.user.role !== "admin")){
 		res.json({msg:  "Sorry" + "Role" + req.user.role})
 	}

    const wasCreated =  await registerBuilding({
    	streetAddress: req.body.streetAddress,
    	unitNumber: req.body.unitNumber,
    	supers: req.body.supers
    },req.user);
     res.json(wasCreated);
}));

router.post("/registerTechnition",mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if (req.user.role !== "technition"){
 		res.json({msg:  "Sorry" + "Role" + req.user.role})
 		return;
 	}
 	const wasCreated =  await registerTechnition({
    	firstName: req.body.firstName,
    	lastName: req.body.lastName,
    	phoneNumber: req.body.phoneNumber,
    	specialities: req.body.specialities,
    	type: req.body.type,
    	email: req.body.email
    },req.user);
    res.json(wasCreated);    


}));


router.post('/registerSuper', mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if (req.user.role !== "super" && (req.user.role !== "admin")){
 		res.json({msg:  "Sorry" + "Role" + req.user.role})
 	}
    console.log(req.body);
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const phone = req.body.phoneNumber;
    const livingAddress = req.body.livingAddress;

    const emailAddress = req.body.email;
    const wasCreated =  await registerBusinessSuper({
    	firstName: firstName,
    	lastName: lastName,
    	phoneNumber: phone,
    	email: emailAddress,
    },req.user);
    res.json(wasCreated);
}));
export default router;