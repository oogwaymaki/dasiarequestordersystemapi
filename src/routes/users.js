import express from 'express';
import { mypassport }  from '../auth/authImplementation';

var router = express.Router();

/* GET users listing. */
router.get('/', mypassport.authenticate('jwt', { session: false }), (req, res) => {
 	res.json({ user: req.user });
});

export default router;
