import express from 'express';
import ash from 'express-async-handler';
import { mypassport }  from '../auth/authImplementation';
import {createWorkOrder,getWorkOrder,getListOfOrders } from '../order/orderSystemFunctionality';

var router = express.Router();
router.post('/createOrder', mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if (req.user.role === null) {
 		res.json({msg:  "Sorry" + "Role" + req.user.role})
 	}

    const wasCreated =  await createWorkOrder({
    unit: req.body.unit,
	streetAddress:req.body.streetAddress,
	type:req.body.type,
	workDescription:req.body.workDescription
},req.user);
    res.json(wasCreated);
}));

router.post('/getOrder/orders', mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if (req.user.role === null) {
 		res.json({msg:  "Sorry" + "Role" + req.user.role})
 	}
    const wasFound =  await getListOfOrders(	req.body ,req.user);
    res.json(wasFound);
}));


router.post('/getOrder/orderId', mypassport.authenticate('jwt', { session: false }), ash(async(req, res)  => {
 	if (req.user.role === null) {
 		res.json({msg:  "Sorry" + "Role" + req.user.role})
 	}
    const wasFound =  await getWorkOrder({ orderId : req.body.orderId },req.user);
    res.json(wasFound);
}));

export default router;