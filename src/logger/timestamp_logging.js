import { createLogger, format, transports, config } from 'winston';


const { label, combine, timestamp , prettyPrint ,colorize} = format;
export const customLogger = createLogger({
  level: 'debug',  
  format: combine(
    label(),
        timestamp(),
        prettyPrint(),
        colorize()
      ),
  transports: [
    new transports.File({ filename: './error.log' , level: 'error'  }),
    new transports.File({ filename: './info.log' , level: 'info'  }),
  ],
  exitOnError: false
});

if (process.env.NODE_ENV !== 'production') {
  customLogger.add(new transports.Console({
    format: format.simple()
  }));
}