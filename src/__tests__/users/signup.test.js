import Chance from "chance";
import { wait_for_user_to_be_added, add_user,register_company } from "../utils";

const chance = new Chance();

describe("Signup /api/auth/", () => {
	var token = "";
	var companyNameRandom = "";
	beforeAll(async () => {
		// create admin
		await add_user({
			username: "DaveAdmin",
			password: "BLAM",
			role: "admin"
		});
		token = await wait_for_user_to_be_added("DaveAdmin", "BLAM");

		// create company // or update
		companyNameRandom = chance.company();
		await register_company(token, {
			companyName: companyNameRandom,
			owner: chance.first() + " " + chance.last(),
			phoneNumber: chance.phone(),
			email: chance.email()
		});
	});
	test("It should add an admin user", async () => {
		const response = await add_user({
			username: chance.last() + "_Admin",
			password: "BLAM",
			companyName: companyNameRandom,
			role: "admin"
		});
		expect(response.statusCode).toBe(200);
	});

	test("It should add an super user", async () => {
		const response = await add_user({
			username: chance.last() + "_Super",
			password: "BLAM",
			companyName: companyNameRandom,
			role: "super"
		});
	});

	test("It should add an resident user", async () => {
		const response = await add_user({
			username: chance.last() + "_Resident",
			password: "BLAM",
			companyName: companyNameRandom,
			role: "resident"
		});
	});
	test("It should add an technition user", async () => {
		const response = await add_user({
			username: chance.last() + "_Technition",
			password: "BLAM",
			companyName: companyNameRandom,
			role: "technition"
		});
		expect(response.statusCode).toBe(200);
	});
});
