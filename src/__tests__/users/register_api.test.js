import Chance from "chance";
import {
	wait_for_user_to_be_added,
	add_user,
	register_company,
	register_building,
	register_resident,
	register_super
	
} from "../utils";

const chance = new Chance();

describe("Signup /api/auth/", () => {
	var token = "";
	var companyNameRandom = "";
	beforeAll(async () => {
		await add_user({
			username: "DaveAdmin",
			password: "BLAM",
			role: "admin"
		});
		token = await wait_for_user_to_be_added("DaveAdmin", "BLAM");
		companyNameRandom = chance.company();
		await register_company(token, {
			companyName: companyNameRandom,
			owner: chance.first() + " " + chance.last(),
			phoneNumber: chance.phone(),
			email: chance.email()
		});
	});

	test("It should register a supertest", async () => {
		let userName = chance.last() + "_Super";
		let responseTest = await add_user({
			username: userName,
			password: "BLAM",
			companyName: companyNameRandom,
			role: "super"
		});

		// get token;
		token = await wait_for_user_to_be_added(userName, "BLAM");
		console.log("TOKEN = " + token);

		const response = await register_super(token, {
			firstName: chance.first(),
			lastName: chance.last(),
			phoneNumber: chance.phone(),
			email: chance.email()
		});

		expect(response.statusCode).toBe(200);
	});

	test("It should be able to register buildings", async () => {
		let superName = chance.last();
		let responseTest = await add_user({
			username: superName,
			password: "BLAM",
			companyName: companyNameRandom,
			role: "super"
		});
		token = await wait_for_user_to_be_added(superName, "BLAM");
		console.log("TOKEN = " + token);

		// register super
		let superEmail = chance.email();
		responseTest = await register_super(token, {
			firstName: chance.first(),
			lastName: chance.last(),
			phoneNumber: chance.phone(),
			email: superEmail
		});

		// register building
		let chanceStreetAddress = chance.address();
		const response = await register_building(token, {
			streetAddress: chanceStreetAddress,
			supers: [superEmail]
		});
		expect(response.statusCode).toBe(200);
	});

	test("It should register a resident", async () => {
		let superName = chance.last() + "_super";
		//setup super

		let responseTest = await add_user({
			username: superName,
			password: "BLAM",
			companyName: companyNameRandom,
			role: "super"
		});

		token = await wait_for_user_to_be_added(superName, "BLAM");
		console.log("TOKEN = " + token);

		// register super
		let superEmail = chance.email();
		responseTest = await register_super(token, {
			firstName: chance.first(),
			lastName: chance.last(),
			phoneNumber: chance.phone(),
			email: superEmail
		});

		// register building
		let chanceStreetAddress = chance.address();
		responseTest = await register_building(token, {
			streetAddress: chanceStreetAddress,
			supers: [superEmail]
		});

		// register resident

		let userName = chance.last() + "_Resident";
		responseTest = await add_user({
			username: userName,
			password: "BLAM",
			companyName: companyNameRandom,
			role: "resident"
		});

		token = await wait_for_user_to_be_added(userName, "BLAM");
		console.log("TOKEN = " + token);

		const response = await register_resident(token, {
			firstName: chance.first(),
			lastName: chance.last(),
			streetAddress: chanceStreetAddress,
			phoneNumber: chance.phone(),
			email: chance.email(),
			unitNumber: chance.integer({ min: 1, max: 600 })
		});

		expect(response.statusCode).toBe(200);
	});
});
