import Chance from "chance";
import {
	wait_for_user_to_be_added,
	add_user,
	register_company
} from "../utils";

const chance = new Chance();

describe("Signup /api/auth/", () => {
	var token = "";
	beforeAll(async () => {
		await add_user({
			username: "DaveAdmin",
			password: "BLAM",
			role: "admin"
		});

		token = await wait_for_user_to_be_added("DaveAdmin", "BLAM");
	});

	test("It should add an admin user", async () => {
		const response = await register_company(token, {
			companyName: chance.company(),
			owner: chance.first() + " " + chance.last(),
			phoneNumber: chance.phone(),
			email: chance.email()
		});

		expect(response.statusCode).toBe(200);
	});
});
