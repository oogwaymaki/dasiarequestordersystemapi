import app from "../server";
import supertest from "supertest";
const request = supertest(app);

const register_company = (token, jsonParams) => {
	return request
		.post("/api/registration/registerCompany")
		.set("Authorization", "JWT " + token) // Works.
		.send(jsonParams);
};
const register_super = (token, jsonParams) => {
	return request
		.post("/api/registration/registerSuper")
		.set("Authorization", "JWT " + token) // Works.
		.send(jsonParams);
};

const register_resident = (token,jsonParams) => {
	return request
			.post("/api/registration/registerResident")
			.set("Authorization", "JWT " + token) // Works.
			.send(jsonParams);

}
const register_building = (token, jsonParams) => {
	return request
		.post("/api/registration/registerBuilding")

		.set("Authorization", "JWT " + token) // Works.
		.send(jsonParams);
};

const add_user = jsonParams => {
	return request.post("/api/auth/signup").send(jsonParams);
};

const wait_for_user_to_be_added = (username, password) => {
	return new Promise(function(resolve, reject) {
		try {
			let tryTillAuthenticated = setInterval(async function() {
				console.log("still running");
				let response = await request
					.post("/api/auth/authenticate")
					.send({
						username: username,
						password: password
					});
				if (clearMyInterval(response.body.token)) {
					resolve(response.body.token);
				}
			}, 500);
			function clearMyInterval(token) {
				if (token) {
					clearInterval(tryTillAuthenticated);
					return true;
				} else {
					return false;
				}
			}
		} catch (ex) {
			console.log(ex);
		}
	});
};

export { wait_for_user_to_be_added, add_user, register_company,register_resident, register_building,register_super};
