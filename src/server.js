// app.js
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
var morgan = require('morgan');
import indexRouter from './routes/index';
import authRouter from './routes/auth';
import userRouter from './routes/users';
import registrationRouter from  './routes/registration';
import orderRouter from  './routes/order';
import { mypassport }  from './auth/authImplementation';

const app = express();
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));
app.use('/', indexRouter);
app.use('/api/auth/',authRouter);
app.use('/users/',userRouter);
app.use('/api/registration',registrationRouter);
app.use('/api/order',orderRouter);
export default app;